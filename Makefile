ASM=nasm
LD=ld
ASMFLAGS=-felf64 -g

lib.o: lib.asm
	$(ASM) $(ASMFLAGS) -o $@ $<

dict.o: dict.asm lib.inc
	$(ASM) $(ASMFLAGS) -o $@ $<

main.o:	main.asm lib.inc colon.inc words.inc
	$(ASM) $(ASMFLAGS) -o $@ $<

main: main.o lib.o dict.o
	$(LD) -o $@ $^

clean:
	rm -f lib.o dict.o main.o main

all: main

.PHONY: clean all
