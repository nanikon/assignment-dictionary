%include "lib.inc"
%include "words.inc"

%define BUFFER_SIZE 256
%define SUCCESS_CODE 0
%define ERROR_CODE 1

extern find_word

section .data
message_too_long: db "Введенная строка слишком длинная", 0
message_not_found: db "Введенный ключ не найден в словаре", 0

global _start

section .text
_start:
    sub rsp, BUFFER_SIZE	; Выделяем на стеке буффер под строку-ключ
    mov rdi, rsp		; Читаем строку-ключ из stdin
    mov rsi, BUFFER_SIZE
    call read_word
    test rax, rax
    je .too_long		; Если не поместилась, выводим соответствующее сообщение

    mov rsi, dict		; Ищем строку в словаре
    mov rdi, rsp
    call find_word
    add rsp, BUFFER_SIZE	; Больше считтанная строка нам не нужна
    test rax, rax
    je .not_found		; Если не нашли, выводим соответствующее сообщение
    mov rdi, rax		; Иначе высчитываем начало вхождения значения и выводим его
    add rdi, 8
    push rdi
    call string_length
    pop rdi
    add rdi, rax
    inc rdi
    call print_string
    mov rdi, SUCCESS_CODE
    call exit
.too_long:
    mov rdi, message_too_long
    add rsp, BUFFER_SIZE
    jmp .end
.not_found:
    mov rdi, message_not_found
.end:
    call print_error
    mov rdi, ERROR_CODE
    call exit
