global find_word

%include "lib.inc"

section .text

; Пробегает по словарю (созданному как связный список) и ищет вхождение переданного ключа
; Аргументы: rdi - указатель на нуль-термированную строку (ключ, который ищется), rsi - указатель на начало словаря
; Возврат: rax - если ключ был найден, то адрес начала вхождения в словарь соответствующего элемента, иначе - 0
find_word:
.loop:
    push rsi
    push rdi
    add rsi, 8
    call string_equals
    pop rdi
    pop rsi
    test rax, rax
    jne .find
    mov rsi, [rsi]
    test rsi, rsi
    jne .loop
    xor rax, rax
    ret
.find:
    mov rax, rsi
    ret
    	
